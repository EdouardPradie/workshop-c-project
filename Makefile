##
## EPITECH PROJECT, 2021
## Makefile
## File description:
## Makefile | https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html#Implicit-Variables
##

CC			= 	gcc
RM			= 	rm -rf

SRC         =   matchstick.c            \
 				usage.c 				\
				errors/main_error.c 	\
				init_free_map.c 		\
				user_move.c 			\
				ai_move.c 				\

SRC_MAIN 	= 	main.c 	\

SRC_TEST 	= 	test.c 				\
				test_usage.c 		\
				test_errors.c 		\
				test_map.c 			\
				test_display.c 		\
				test_user.c 		\
				test_ai.c 			\
				test_matchstick.c 	\

DIR_SRC 	= 	src/
DIR_TEST 	= 	tests/

OBJ 		= 	$(addprefix $(DIR_SRC), $(SRC:.c=.o)) $(addprefix $(DIR_SRC), $(SRC_MAIN:.c=.o))

NAME        =   matchstick

CFLAGS		=	-Wall -Werror -Wextra
LDFLAGS		= 	-L./lib -lmy
CPPFLAGS	=	-I./include -I./lib/include

all: 		$(NAME)

$(NAME): 	lib $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS) $(CPPFLAGS) $(CFLAGS)

lib:
	make -C lib/

clean:
	$(RM) $(OBJ)

fclean: 	clean
	$(RM) $(NAME)
	make -C lib/ fclean

re:			fclean all

tests_run: 	re
	gcc -o unit_tests $(addprefix $(DIR_SRC), $(SRC)) $(addprefix $(DIR_TEST), $(SRC_TEST)) $(LDFLAGS) $(CPPFLAGS) --coverage -lcriterion
	./unit_tests

.PHONY: all lib clean fclean re tests_run
