/*
** EPITECH PROJECT, 2020
** my_int_to_str
** File description:
** Put int into a str and return it
*/

#include <stdlib.h>

int my_nbrlen(int nbr);
char *my_revstr(char *str);

int abs(int nbr)
{
    if (nbr < 0)
        nbr *= -1;
    return nbr;
}

char *my_int_to_str(int nbr)
{
    int i = 0;
    int neg = 0;
    char *str = malloc(sizeof(char) * (my_nbrlen(nbr) + 1));

    if (str == 0)
        return "-1";
    if (nbr < 0)
        neg = 1;
    nbr = abs(nbr);
    while (nbr > 9) {
        str[i] = ((nbr % 10) + 48);
        nbr /= 10;
        i++;
    }
    str[i] = nbr + 48;
    if (neg == 1) {
        str[i + 1] = '-';
        i++;
    }
    str[i + 1] = '\0';
    return my_revstr(str);
}
