/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Errors prototypes
*/

#ifndef ERRORS_H
#define ERRORS_H

#include "macros.h"

ret_t check_errors(int ac, const char **av);

#endif /* ERRORS_H */
