/*
** EPITECH PROJECT, 2021
** Main
** File description:
** Main file of Matchstick project
*/

#include "macros.h"
#include "matchstick.h"

int main(int ac, const char **av)
{
    return (matchstick(ac, av));
}
