/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** matchstick
*/

#include <stdio.h>
#include <stdlib.h>
#include "mystring.h"
#include "mymath.h"
#include "usage.h"
#include "errors.h"
#include "matchstick.h"

ret_t check_line(short lines, const char *line)
{
    int nbr = my_getnbr(line);

    if (!my_str_isnum(line) || nbr < 0)
        return my_print_int_error(ERROR_INVALID_INPUT, 1, ERROR);
    if (nbr < 1 || nbr > lines)
        return my_print_int_error(ERROR_INVALID_LINE, 1, ERROR);
    return SUCCESS;
}

short get_player_line(short lines)
{
    char *line = NULL;
    size_t len = 0;
    short ret;

    my_putstr("Line: ");
    if (getline(&line, &len, stdin) == -1) {
        free(line);
        return SPECIFIC;
    }
    line[my_strlen(line) - 1] = '\0';
    if (!check_line(lines, line)) {
        free(line);
        return ERROR;
    }
    ret = my_getnbr(line);
    free(line);
    return ret;
}

ret_t check_matches(short matches, short free_in_line, const char *line)
{
    int nbr = my_getnbr(line);

    if (!my_str_isnum(line) || nbr < 0)
        return my_print_int_error(ERROR_INVALID_INPUT, 1, ERROR);
    if (nbr == 0)
        return my_print_int_error(ERROR_TOOLESS_MATCHES, 1, ERROR);
    if (nbr > matches) {
        my_putstr("Error: you cannot remove more than ");
        my_put_nbr(matches);
        return my_print_int_error(" matches per turn\n", 1, ERROR);
    }
    if (nbr > free_in_line)
        return my_print_int_error(ERROR_TOOMUCH_MATCHES, 1, ERROR);
    return SUCCESS;
}

short get_player_matches(short matches, const char *map_line)
{
    short free_in_line = 0;
    short ret;
    char *line = NULL;
    size_t len = 0;

    for (int i = 0; map_line[i]; i++)
        if (map_line[i] == STICK_CHAR)
            free_in_line++;
    my_putstr("Matches: ");
    if (getline(&line, &len, stdin) == -1) {
        free(line);
        return SPECIFIC;
    }
    line[my_strlen(line) - 1] = '\0';
    if (!check_matches(matches, free_in_line, line)) {
        free(line);
        return ERROR;
    }
    ret = my_getnbr(line);
    free(line);
    return ret;
}

void get_player_reads(game_t *game)
{
    short line = ERROR;
    short matches = ERROR;

    my_putstr("\nYour turn:\n");
    while (!line || !matches) {
        line = get_player_line(game->lines);
        if (line == SPECIFIC) {
            game->cur_player = EXIT;
            return;
        }
        if (line > 0)
            matches = get_player_matches(game->matches, (game->map)[line - 1]);
        if (matches == SPECIFIC) {
            game->cur_player = EXIT;
            return;
        }
    }
    game->cur_player = AI;
    my_putstr("Player");
    do_remove(&((game->map)[line - 1]), line, matches);
}
